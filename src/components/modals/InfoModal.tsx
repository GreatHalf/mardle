import { Cell } from '../grid/Cell'
import { BaseModal } from './BaseModal'

type Props = {
  isOpen: boolean
  handleClose: () => void
}

export const InfoModal = ({ isOpen, handleClose }: Props) => {
  return (
    <BaseModal title="How to play" isOpen={isOpen} handleClose={handleClose}>
      <p className="text-sm text-gray-500 dark:text-gray-300">
        Guess the Mario game title (or spin-off title) in 3 tries. After each guess, the color of the tiles will
        change to show how close your guess was to the word.
      </p>

      <div className="flex justify-center mb-1 mt-4">
        <Cell
          isRevealing={true}
          isCompleted={true}
          value="M"
          status="correct"
        />
        <Cell value="K" />
        <Cell value="6" />
      </div>
      <p className="text-sm text-gray-500 dark:text-gray-300">
        The word Mario is in the game title and in the correct spot.
      </p>

      <div className="flex justify-center mb-1 mt-4">
        <Cell value="d" />
        <Cell value="k" />
		<Cell
          isRevealing={true}
          isCompleted={true}
          value="L"
          status="present"
        />
      </div>
      <p className="text-sm text-gray-500 dark:text-gray-300">
        The word Land is in the game title but in the wrong spot.
      </p>

      <div className="flex justify-center mb-1 mt-4">
        <Cell value="S" />
        <Cell value="M" />
        <Cell isRevealing={true} isCompleted={true} value="P" status="absent" />
      </div>
      <p className="text-sm text-gray-500 dark:text-gray-300">
        The word Party is not in the game title in any spot.
      </p>

      <p className="mt-6 italic text-sm text-gray-500 dark:text-gray-300">
        This is a fork from the open source version of the word guessing game we all know and
        love -{' '}
        <a
          href="https://github.com/cwackerfuss/react-wordle"
          className="underline font-bold"
        >
          check out the code here
        </a>{' '}
      </p>
    </BaseModal>
  )
}
