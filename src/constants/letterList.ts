export const letterToGameWordMap: { [id: string]: string } = {
 '2':  '2/II',
'3':  '3',
'4':  '4',
'7':  '7',
'8':  '8',
'6': '64',
'A':  'Advance',
'D':  'DS',
'd':  'Donkey',
'R':  'Dr.',
's':  'Stealth',
'K':  'Kart',
'k':  'Kong',
'L':  'Land',
'M':  'Mario',
'P':  'Party',
'S': 'Super',
'W': 'Wario',
'w': 'World'
}

export const convertValueToGameWord = (pValue: string | undefined) => {
	  if (pValue !== undefined) {
		return letterToGameWordMap[pValue]
	  }
	  return ''
	}
